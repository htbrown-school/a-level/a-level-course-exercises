print("End Of Year Exam Percentage Application")
student_name = input("Enter student name: ") 

total_marks = 100
subjects = ["Computer Science", "Maths", "English", "MFL", "Geography", "Biology", "Physics", "Sociology"]

for s in subjects:
    marks = int(input(f"Enter total marks for {s} paper: "))
    percentage = (marks / total_marks) * 100

    if percentage > 90:
        grade = "A*"
    elif percentage > 80:
        grade = "A"
    elif percentage > 70:
        grade = "B"
    elif percentage > 60:
        grade = "C"
    else:
        grade = "Fail"

    if percentage > 60:
        print(f"Congratulations, {student_name} has scored {marks}, secured {percentage}% and has achieved grade {grade}.")
    else:
        print(f"Sorry, {student_name} has failed.")
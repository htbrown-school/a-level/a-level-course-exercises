stations = ["Brixton", "Stockwell", "Vauxhall", "Pimlico", "Victoria", "Green Park", "Oxford Circus", "Warren Street", "Euston", "King's Cross", "Highbury & Islington", "Finsbury Park", "Seven Sisters", "Tottenham Hale", "Blackhorse Road", "Walthamstow Central"]
print(stations)
print()

start = input("Enter starting station: ")
end = input("Enter ending station: ")
print()

while not (start in stations and end in stations):
    print("Invalid station entered.")

    start = input("Enter starting station: ")
    end = input("Enter ending station: ")
    print()

if stations.index(start) > stations.index(end):
    difference = (stations.index(start) - stations.index(end))
else:
    difference = (stations.index(end) - stations.index(start))
    
print(f"There are {difference} stops between {start} and {end}.") 
import random

board = [[0 for j in range(8)] for i in range(8)]
tanks = 0

while tanks < 10: 
    row = random.randrange(0, 7)
    col = random.randrange(0, 7)
    
    if board[col][row] == 0:
        board[col][row] = 1
        tanks += 1

turns = 0
print(board) 

while tanks > 0 and turns < 50:
    row = int(input("Enter row: "))
    col = int(input("Enter column: "))

    while (row > 7 or col > 7) and (row.isdigit() and col.isdigit()):
        print("Invalid reference. Make sure you enter numbers in the range 0-7.")
        row = int(input("Enter row: "))
        col = int(input("Enter column: "))

    if board[col][row] == 1:
        print(f"Tank destroyed at position ({row}, {col}).")
        board[col][row] = 0
        tanks = tanks - 1
    else:
        print("No tank at that location.")
    turns += 1   

if tanks > 0:
    print("Game over. You didn't manage to destroy all the tanks.")
else:
    print("You won! All the tanks were destroyed")
usernames_file = open("usernames.txt", "r") 

passwords_file = open("passwords.txt", "r") 

 

usernames = usernames_file.read().splitlines()
passwords = passwords_file.read().splitlines()

usernames_file.close()
passwords_file.close()

username = input("Enter username: ")
password = input("Enter password: ")

while username not in usernames or passwords[usernames.index(username)] != password:
    print("Invalid username or password.")
    username = input("Enter username: ")
    password = input("Enter password: ")
    
print(f"Hello, {username}.") 
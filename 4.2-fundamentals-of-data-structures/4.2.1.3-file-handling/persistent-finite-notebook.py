import os.path
import json

def viewNotes(notes):
    for i, v in enumerate(notes):
           print(i, ":", v)

def changeNote(notes):
    note = int(input("Enter note to modify: "))
    content = input("Enter new contents of note: ")
    notes[note] = content
    print("Note changed.")

def deleteNote(notes):
    note = int(input("Enter note to delete: "))
    notes[note] = ""
    print("Note deleted.")

def saveNotes(notes):
    file = open("notes.json", "w")
    json.dump(notes, file)
    file.close() 

notes = ["" for i in range(10)]
if os.path.isfile("notes.json"):
    file = open("notes.json", "r")
    notes = json.load(file)
    file.close()

print("""MENU
1. View Notes
2. Change Note
3. Delete Note
4. Quit""")
print()

choice = 0

while choice != 4:
    choice = int(input("Enter an option: "))
    print()
    if choice == 1:
        viewNotes(notes)
    elif choice == 2:
        changeNote(notes)
    elif choice == 3:
        deleteNote(notes)
    elif choice == 4:
        saveNotes(notes)
        exit() 

    print()
    print("""MENU
1. View Notes
2. Change Note
3. Delete Note
4. Quit""")
    print() 
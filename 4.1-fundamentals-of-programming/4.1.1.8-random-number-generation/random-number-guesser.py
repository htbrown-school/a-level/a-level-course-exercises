import random

number = random.randrange(1, 100)
guess = int(input("Enter guess: "))
print()

while guess != number:
    if guess > number:
        print("Incorrect. Your number is too high.")
    else:
        print("Incorrect. Your number is too low.")

    print()
    guess = int(input("Enter guess: "))
    print() 

print("You guessed the correct number.") 
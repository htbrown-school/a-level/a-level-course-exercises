def palindrome(string):
    string = [i for i in string if i.isalnum()]
    if len(string) <= 1:
        return True
    elif string[0] == string[len(string) - 1]:
        del string[0]
        del string[len(string) - 1]
        return palindrome(''.join(string))
    else:
        return False

sentence = input("Enter sentence: ")
print(palindrome(sentence)) 